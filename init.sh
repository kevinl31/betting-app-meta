#!/bin/bash

echo '-- Init betting project --'

mkdir services 

echo '-- Configuring api --'

git clone https://gitlab.com/kevinl31/betting-app-frontend.git ./services/api/
cd services/api/
npm install
npm run test

echo '-- Configuring api DONE --'

cd ../..

echo '-- Configuring frontend --'

git clone https://gitlab.com/kevinl31/betting-app-frontend.git ./services/frontend/
cd services/frontend/
npm install
npm run test

echo '-- Configuring frontend DONE --'

cd ../..

echo '-- Launching docker-compose --'

docker-compose up -d --build

echo '-- Project is ready. Go to http://localhost:4200/ to see the project ;) --'